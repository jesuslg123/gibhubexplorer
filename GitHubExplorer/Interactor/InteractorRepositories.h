//
//  InteractorRepositories.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Repository;

@interface InteractorRepositories : NSObject


/**
 Fetch all repositories

 @param block completionBlock
 */
- (void)fetchRepositoriesWithCompletionBlock:(void (^_Nullable)(NSError * _Nullable error, NSArray <Repository *>* _Nullable result))block;


/**
 Fectch repositories with pagination

 @param aPage NSNumber Page
 @param block completionBlock
 */
- (void)fetchRepositoriesAtPage:(nullable NSNumber *)aPage withCompletionBlock:(void (^_Nullable)(NSError * _Nullable error, NSArray <Repository *>* _Nullable result))block;

@end
