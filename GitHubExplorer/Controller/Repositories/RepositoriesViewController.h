//
//  RepositoriesViewController.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionView.h"

@interface RepositoriesViewController : UIViewController

@property (strong, nonatomic, nullable) CollectionView *collectionView;

@end

