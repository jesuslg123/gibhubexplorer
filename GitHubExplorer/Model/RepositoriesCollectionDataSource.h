//
//  RepositoriesCollectionDataSource.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Repository;

@interface RepositoriesCollectionDataSource : NSObject <UICollectionViewDataSource>

@property (strong, nonatomic, nullable) NSArray <Repository *>*repositories;

@property (strong, nonatomic, nullable) NSNumber *nextPage;
@property (nonatomic) BOOL arePendingItems;


/**
 Increment the next page index
 */
- (void)incrementPage;

@end
