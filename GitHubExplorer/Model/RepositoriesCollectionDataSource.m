//
//  RepositoriesCollectionDataSource.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "RepositoriesCollectionDataSource.h"

#import "CollectionViewCell.h"

@implementation RepositoriesCollectionDataSource

- (instancetype)init {
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.arePendingItems = YES;
    self.nextPage = @(1);
}

#pragma mark - Pagination

- (void)incrementPage {
    self.nextPage = @(self.nextPage.integerValue+1);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(nonnull UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return (self.repositories != nil ? self.repositories.count : 0);
            break;
            
        default:
            return 0;
            break;
    }
}

- (nonnull CollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[CollectionViewCell cellIdentifier] forIndexPath:indexPath];
    
    [cell configureCellWithRepository:[self repositoryAtIndexPath:indexPath]];
    
    return cell;
}

#pragma mark -

- (nonnull Repository *)repositoryAtIndexPath:(nonnull NSIndexPath *)aIndexPath {
    return (self.repositories != nil && aIndexPath.row < self.repositories.count ? self.repositories[aIndexPath.row] : nil);
}

@end
