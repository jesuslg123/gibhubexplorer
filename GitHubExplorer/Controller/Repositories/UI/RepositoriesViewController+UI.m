//
//  RepositoriesViewController+UI.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "RepositoriesViewController+UI.h"

@implementation RepositoriesViewController (UI)

#pragma mark - Initialize

- (void)initializeUI {
    [self initializeMainView];
    [self initializeCollectionView];
}

- (void)initializeMainView {
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.view.backgroundColor = [UIColor lightGrayColor];
    [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
}

#pragma mark - Configure

- (void)configureUI {
    [self configureConstraints];
}

- (void)configureConstraints {
    [self configureConstraintsCollectionView];
    [self updatePositions];
}

#pragma mark - CollectionView

- (void)initializeCollectionView {
    self.collectionView = [CollectionView basicCollectionView];
    [self.view addSubview:self.collectionView];
}

#pragma mark Constraints

- (void)configureConstraintsCollectionView {
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeTop multiplier:1 constant:-10];
    
    NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.collectionView attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    
    NSLayoutConstraint *traillingConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.collectionView attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    
    [self.view addConstraints:@[topConstraint, bottomConstraint, leadingConstraint, traillingConstraint]];
}

- (void)updatePositions {
    [self.view updateConstraintsIfNeeded];
}

@end
