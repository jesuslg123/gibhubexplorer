//
//  Repository.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "ObjectBase.h"

@class RKMapping;

@interface Repository : ObjectBase

@property (strong, nonatomic, nullable) NSString *name;
@property (strong, nonatomic, nullable) NSString *descriptionText;
@property (strong, nonatomic, nullable) NSString *loginOwner;
@property (nonatomic) BOOL isFork;


/**
 Get RestKit mapping

 @return RKMapping  Object mapping
 */
+ (nonnull RKMapping *)mapping;

@end
