//
//  ObjectBase.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 18/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "ObjectBase.h"

@implementation ObjectBase

+ (NSArray *)fetchAll {
    return (NSArray *)[[self class] allObjects];
}

- (RLMRealm *)realm {
    RLMRealm *realm = [RLMRealm defaultRealm];
    return realm;
}

- (void)save {
    RLMRealm * realm = [self realm];
    [realm transactionWithBlock:^{
        [realm addObject:self];
    }];
}

- (void)delete {
    RLMRealm * realm = [self realm];
    [realm transactionWithBlock:^{
        [realm deleteObject:self];
    }];
}

@end
