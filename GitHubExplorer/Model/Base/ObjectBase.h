//
//  ObjectBase.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 18/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface ObjectBase : RLMObject


/**
 Get all stored type instances

 @return NSArray Stored instances
 */
+ (NSArray *)fetchAll;


/**
 Store the instance
 */
- (void)save;


/**
 Remove stored the instance
 */
- (void)delete;

@end
