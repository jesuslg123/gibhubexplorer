//
//  InteractorRepositories.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "InteractorRepositories.h"
#import "Repository.h"
#import "RepositoriesNetworkRquest.h"


@implementation InteractorRepositories

- (void)fetchRepositoriesWithCompletionBlock:(void (^)(NSError * error, NSArray <Repository *>*result))block {
    [self fetchRepositoriesAtPage:nil withCompletionBlock:block];
}

- (void)fetchRepositoriesAtPage:(nullable NSNumber *)aPage withCompletionBlock:(void (^)(NSError * error, NSArray <Repository *>*result))block {
    //TODO: Check persisted data for caching
    
    [self fetchRemoteRepositoriesAtPage:aPage block:block];
}

- (void)fetchRemoteRepositoriesAtPage:(nullable NSNumber *)aPage block:(void (^ _Nullable)(NSError *, NSArray<Repository *> *))block {
    NSMutableDictionary *arguments = [NSMutableDictionary new];
    
    if (aPage != nil) {
        [arguments setObject:[NSString stringWithFormat:@"%@", aPage] forKey:@"page"];
    }
    
    [[RepositoriesNetworkRquest new] requestRepositoriesWithArguments:arguments completionBlock:^(id error, NSArray<Repository *> *result) {
        if (aPage.integerValue == 1) {
            [self cleanPersistedData];
        }
        [self persistRepositories:result];
        block(error, result);
    }];
}

- (void)persistRepositories:(NSArray<Repository *> *)aRepositories {
    for (Repository *repository in aRepositories) {
        [repository save];
    }
}

- (void)cleanPersistedData {
    NSArray *all = [Repository fetchAll];
    for (Repository *repository in all) {
        [repository delete];
    }
}

#pragma mark - Mock data for testing

- (NSArray <Repository *>*)mockData {
    NSMutableArray <Repository *>*mocks = [NSMutableArray new];
    
    for (int i=0; i<30; i++) {
        [mocks addObject:[self mockRepository:i]];
    }
    return mocks.copy;
}

- (Repository *)mockRepository:(int)number {
    Repository *repo = [Repository new];
    
    repo.name = [NSString stringWithFormat:@"Name - %d", number];
    repo.descriptionText = [NSString stringWithFormat:@"This is the repo description text, this could be very long - %d", number];
    repo.loginOwner = [NSString stringWithFormat:@"Owner %d", number];
    repo.isFork = number%2;
    
    return repo;
}

@end
