//
//  NetworkManager.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RKObjectRequestOperation, RKMappingResult, RKMapping;

@interface NetworkManager : NSObject

@property (strong, nonatomic, nullable) NSString *host;
@property (strong, nonatomic, nullable) NSString *token;


/**
 Intialize with host and token

 @param aHost NSString Host
 @param aToken NSString Access token
 @return instancetype
 */
- (nullable instancetype)initWithHost:(nonnull NSString *)aHost token:(nullable NSString *)aToken;


/**
 Get request

 @param aEndPoint NSString End point
 @param aArguments NSDictionary Request params
 @param aResponseMapping RKMapping Response object mapping
 @param success block Success
 @param failure block Fail
 */
- (void) getWithEndPoint:(nonnull NSString *)aEndPoint arguments:(nullable NSDictionary *)aArguments responseMapping:(nonnull RKMapping *)aResponseMapping success:(void (^_Nullable)(RKObjectRequestOperation * _Nullable operation, RKMappingResult * _Nullable mappingResult))success failure:(void (^_Nullable)(RKObjectRequestOperation * _Nullable operation, NSError * _Nullable error))failure;


@end
