//
//  RepositoriesViewController.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "RepositoriesViewController.h"
#import "RepositoriesViewController+UI.h"

#import "CollectionViewCell.h"
#import "RepositoriesCollectionDataSource.h"
#import "InteractorRepositories.h"
#import "Repository.h"

@interface RepositoriesViewController () <UICollectionViewDelegate>

@property (strong, nonatomic, nullable) RepositoriesCollectionDataSource *dataSource;
@property (strong, nonatomic, nullable) NSArray *repositories;

@property (nonatomic) BOOL isFetching;

@end

@implementation RepositoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initializeUI];
    [self initialize];
    [self configureUI];
}


#pragma mark - Initialize

- (void)initialize {
    [self.collectionView setDelegate:self];
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:[CollectionViewCell cellIdentifier]];
    
    self.dataSource = [RepositoriesCollectionDataSource new];
    
    [self fetchData:self.dataSource.nextPage];
}

#pragma mark - Data Manage

- (void)fetchData:(nonnull NSNumber *)atPage {
    if (self.isFetching || !self.dataSource.arePendingItems) {
        return;
    }
    
    self.isFetching = YES;
    [[InteractorRepositories new] fetchRepositoriesAtPage:atPage withCompletionBlock:^(NSError *error, NSArray<Repository *> *result) {
        if (error == nil) {
            [self processNewData:result];
        } else {
            [self showErrorWithTitle:@"Ops!" message:error.localizedDescription];
        }
        self.isFetching = NO;
    }];
}

- (void)processNewData:(NSArray<Repository *> *)result {
    if (result.count > 0) {
        [self.dataSource incrementPage];
        self.dataSource.arePendingItems = YES;
    } else {
        self.dataSource.arePendingItems = NO;
    }
    [self updateDataSource:result];
}

- (void)updateDataSource:(nullable NSArray <Repository *>*)aArray {
    NSMutableArray *currentRepositories = [NSMutableArray arrayWithArray:self.dataSource.repositories];
    [currentRepositories addObjectsFromArray:aArray];
    
    [self.dataSource setRepositories:currentRepositories];
    [self.collectionView setDataSource:self.dataSource];
    [self.collectionView reloadData];
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > self.dataSource.repositories.count - 4) {
        [self loadNextPage];
    }
}

- (void)loadNextPage {
    [self fetchData:self.dataSource.nextPage];
}

#pragma mark - Error handling

- (void)showErrorWithTitle:(nullable NSString *)aTitle message:(nonnull NSString *)aMessage {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:aTitle message:aMessage preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil] ;
}

@end
