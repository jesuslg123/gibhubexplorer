//
//  CollectionView.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionView : UICollectionView


/**
 Get basic collection view instance

 @return CollectionView CollectionView instance
 */
+ (CollectionView *_Nonnull)basicCollectionView;

@end
