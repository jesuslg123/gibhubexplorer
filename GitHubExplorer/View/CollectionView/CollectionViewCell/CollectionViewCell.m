//
//  CollectionViewCell.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "CollectionViewCell.h"
#import "Repository.h"


@interface CollectionViewCell()

@property (strong, nonatomic, nullable) UILabel *labelName;
@property (strong, nonatomic, nullable) UILabel *labelDescription;
@property (strong, nonatomic, nullable) UILabel *labelLoginOwner;

@end


@implementation CollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self initializeUI];
    }
    
    return self;
}


+ (NSString *)cellIdentifier {
    return @"RepositoryCollectionCell";
}


#pragma mark - Configure UI

- (void) initializeUI {
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self initializeLabelNameUI];
    [self initializeLabelDescriptionUI];
    [self initializeLabelLoginOwnerUI];
    
    [self buildStructureUI];
}

- (void)buildStructureUI {
    UIStackView *verticalStackView = [[UIStackView alloc] initWithArrangedSubviews:@[self.labelName, self.labelDescription, self.labelLoginOwner]];
    
    [verticalStackView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [verticalStackView setAxis:UILayoutConstraintAxisVertical];
    [verticalStackView setSpacing:10];
    
    [self addSubview:verticalStackView];
    [self configureConstraintsStackView:verticalStackView];
    [self configureConstraintsLabels];
    [self layoutIfNeeded];
}

- (void) initializeLabelNameUI {
    self.labelName = [UILabel new];
    [self.labelName setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.labelName setNumberOfLines:0];
    [self.labelName setTextAlignment:NSTextAlignmentCenter];
    [self.labelDescription setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
}

- (void) initializeLabelDescriptionUI {
    self.labelDescription = [UILabel new];
    [self.labelDescription setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.labelDescription setNumberOfLines:0];
    [self.labelDescription setTextAlignment:NSTextAlignmentLeft];
    [self.labelDescription setMinimumScaleFactor:0.5];
    [self.labelDescription setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
}

- (void) initializeLabelLoginOwnerUI {
    self.labelLoginOwner = [UILabel new];
    [self.labelLoginOwner setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.labelLoginOwner setNumberOfLines:1];
    [self.labelLoginOwner setTextAlignment:NSTextAlignmentRight];
    [self.labelDescription setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
}

#pragma mark - Constraints

- (void)configureConstraintsStackView:(nonnull UIStackView *)aStackView {
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:aStackView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:10];
    
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:aStackView attribute:NSLayoutAttributeBottom multiplier:1 constant:10];
    
    NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:aStackView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:10];
    
    NSLayoutConstraint *traillingConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:aStackView attribute:NSLayoutAttributeTrailing multiplier:1 constant:10];
    
    [self addConstraints:@[topConstraint, bottomConstraint, leadingConstraint, traillingConstraint]];
}

- (void)configureConstraintsLabels {
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.labelDescription attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationLessThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:300];
    [self.labelDescription addConstraint:widthConstraint];
}

#pragma mark - Configure

- (void)configureCellWithRepository:(Repository *)aRepository {
    [self updateName:aRepository.name];
    [self updateDescription:aRepository.descriptionText];
    [self updateLoginOwner:aRepository.loginOwner];
    [self updateIsForked:aRepository.isFork];
}

- (void)updateName:(nonnull NSString *)aName {
    [self.labelName setText:aName];
}

- (void)updateDescription:(nonnull NSString *)aName {
    [self.labelDescription setText:aName];
}

- (void)updateLoginOwner:(nonnull NSString *)aName {
    [self.labelLoginOwner setText:aName];
}

- (void)updateIsForked:(BOOL)aBool {
    [self setBackgroundColor:(aBool ? [UIColor greenColor] : [UIColor whiteColor])];
}

@end
