//
//  Repository.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "Repository.h"
#import <RestKit/RestKit.h>

@implementation Repository

#pragma mark - Mapping

+ (RKMapping *)mapping {
    
    
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[self class]];
    
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"name": @"name",
                                                  @"description": @"descriptionText",
                                                  @"owner.login": @"loginOwner",
                                                  @"fork": @"isFork"
                                                  }];
    
    return mapping;
    
}

@end
