//
//  NetworkManager.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "NetworkManager.h"

#import <RestKit/RestKit.h>

@interface NetworkManager()

@property (strong, nonatomic, nullable) RKObjectRequestOperation *operation;

@end



@implementation NetworkManager


- (nullable instancetype)initWithHost:(NSString *)aHost token:(NSString *)aToken {
    if(self = [super init]){
        self.host = aHost;
        self.token = aToken;
    }
    return self;
}

- (RKResponseDescriptor *)generateResponseDescriptorWithKeyPath:(NSString *)aKeyPath pathPattern:(NSString *)aPathPattern mapping:(RKMapping *)aMapping {
    RKResponseDescriptor *playerDealsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:aMapping
                                                                                                       method:RKRequestMethodGET
                                                                                                  pathPattern:aPathPattern
                                                                                                      keyPath:aKeyPath
                                                                                                  statusCodes:[NSIndexSet indexSetWithIndex:200]];
    return playerDealsResponseDescriptor;
}

- (NSMutableURLRequest *)requestWithURL:(NSURL *)aUrl {
    return [NSMutableURLRequest requestWithURL:aUrl];
}


- (void) getWithEndPoint:(NSString *)aEndPoint arguments:(NSDictionary *)aArguments responseMapping:(RKMapping *)aResponseMapping
                 success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                 failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure {
    
    [self getWithEndPoint:aEndPoint arguments:aArguments pathPattern:aEndPoint responseMapping:aResponseMapping success:success failure:failure];
    
}

- (void) getWithEndPoint:(NSString *)aEndPoint arguments:(NSDictionary *)aArguments pathPattern:(NSString *)aPathPattern responseMapping:(RKMapping *)aResponseMapping success: (void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure {
    
    [self initializeOperation:aArguments aEndPoint:aEndPoint aPathPattern:aPathPattern aResponseMapping:aResponseMapping];
    
    [self setOperationBlocks:failure success:success];
    
    [self.operation start];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}

- (NSMutableURLRequest *)createRequest:(NSDictionary *)aArguments aEndPoint:(NSString *)aEndPoint {
    NSURL *url = [self fullURLWithHost:self.host endPoint:aEndPoint arguments:aArguments];
    NSMutableURLRequest *request = [self requestWithURL:url];
    return request;
}

- (void)initializeOperation:(NSDictionary *)aArguments aEndPoint:(NSString *)aEndPoint aPathPattern:(NSString *)aPathPattern aResponseMapping:(RKMapping *)aResponseMapping {
    RKResponseDescriptor *responseDescriptor = [self generateResponseDescriptorWithKeyPath:@"" pathPattern:aPathPattern mapping:aResponseMapping];
    
    NSMutableURLRequest * request = [self createRequest:aArguments aEndPoint:aEndPoint];
    self.operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    self.operation.HTTPRequestOperation.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
}

- (void)setOperationBlocks:(void (^)(RKObjectRequestOperation *, NSError *))failure success:(void (^)(RKObjectRequestOperation *, RKMappingResult *))success {
    [self.operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operationResponse, RKMappingResult *mappingResult) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        success(operationResponse, mappingResult);
    } failure:^(RKObjectRequestOperation *operationResponse, NSError *error) {
        NSLog(@"ERROR: %@", error);
        NSLog(@"Response: %@", operationResponse.HTTPRequestOperation.responseString);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failure(operationResponse, error);
    }];
}

- (NSURL *)fullURLWithHost:(NSString *)aHost endPoint:(NSString *)aEndPoint arguments:(NSDictionary *)aArguments {
    NSURLComponents *components = [NSURLComponents componentsWithString:aHost];
    components.path = aEndPoint;
    
    aArguments = [self appendToken:aArguments];
    
    if(aArguments != nil){
        components.queryItems = [self processArguments:aArguments].copy;
    }
    
    return components.URL;
}

#pragma mark - Arguments

- (NSMutableArray *)processArguments:(NSDictionary *)aArguments {
    NSMutableArray *arguments = [NSMutableArray new];
    for (int i=0; i<[aArguments allKeys].count; i++) {
        NSString *key = [aArguments allKeys][i];
        NSString *value = [aArguments objectForKey:key];
        value = [self URLencodeString:value];
        [arguments addObject:[NSURLQueryItem queryItemWithName:key value:value]];
    }
    return arguments;
}

- (NSString *)URLencodeString:(NSString *)aString {
    return [aString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
}

- (nonnull NSDictionary *)appendToken:(nullable NSDictionary *)aDictionary {
    if (self.token != nil) {
        NSMutableDictionary *arguments = [NSMutableDictionary new];
        if (aDictionary != nil) {
             arguments = [NSMutableDictionary dictionaryWithDictionary:aDictionary];
        }
        [arguments setObject:self.token forKey:@"access_token"];
        return arguments.copy;
    } else {
        return nil;
    }
}

@end
