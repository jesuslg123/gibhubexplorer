//
//  RepositoriesNetworkManager.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "RepositoriesNetworkRquest.h"
#import "NetworkManager.h"
#import "Repository.h"
#import <RestKit/RestKit.h>

@implementation RepositoriesNetworkRquest

- (void)requestRepositoriesWithArguments:(NSDictionary *)aArguments completionBlock:(void (^)(id error, NSArray <Repository *>*result))block {
    NetworkManager *networkManager = [[NetworkManager alloc] initWithHost:@"https://api.github.com" token:@"6106484510f7f2542a8b810326b6595a9885d0d9"];
    
    [networkManager getWithEndPoint:@"/users/xing/repos" arguments:aArguments responseMapping:[Repository mapping] success:^(RKObjectRequestOperation * _Nullable operation, RKMappingResult * _Nullable mappingResult) {
        NSArray <Repository *> *repositories = (NSArray <Repository *>*)mappingResult.array;
        block(nil, repositories);
    } failure:^(RKObjectRequestOperation * _Nullable operation, NSError * _Nullable error) {
        block(error, nil);
    }];
}

@end
