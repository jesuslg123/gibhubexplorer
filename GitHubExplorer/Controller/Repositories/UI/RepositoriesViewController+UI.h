//
//  RepositoriesViewController+UI.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//


#import "RepositoriesViewController.h"

@interface RepositoriesViewController (UI)


/**
 Initialize all UI elements
 */
- (void)initializeUI;


/**
 Configure all UI elements design
 */
- (void)configureUI;

@end
