//
//  RepositoriesCollectionDataSourceTests.m
//  GitHubExplorerTests
//
//  Created by Jesus Lopez Garcia on 18/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RepositoriesCollectionDataSource.h"
#import "Repository.h"

@interface RepositoriesCollectionDataSource()

- (nonnull Repository *)repositoryAtIndexPath:(nonnull NSIndexPath *)aIndexPath;

@end

@interface RepositoriesCollectionDataSourceTests : XCTestCase

@property (strong, nonatomic, nullable) RepositoriesCollectionDataSource *dataSource;

@end

@implementation RepositoriesCollectionDataSourceTests

- (void)setUp {
    [super setUp];
    
    [self initialize];
}

- (void)tearDown {
    [self finish];
    
    [super tearDown];
}

- (void)initialize {
    self.dataSource = [RepositoriesCollectionDataSource new];
}

- (void)finish {
    self.dataSource = nil;
}

#pragma mark - Tests

#pragma mark IncrementPage

- (void)testWhenIncrementPage_ThenGetPageIncrementByOne {
    NSNumber *initialValue = self.dataSource.nextPage;
    
    [self.dataSource incrementPage];
    
    NSNumber *nextValue = self.dataSource.nextPage;
    
    XCTAssertGreaterThan(nextValue.integerValue, initialValue.integerValue, @"Next page is not greater than original value");
    XCTAssertEqual(nextValue.integerValue, initialValue.integerValue+1, @"Next page is not equal to initial value plus 1");
}

- (void)testWhenNoIncrementPage_ThenGetSamePage {
    NSNumber *initialValue = self.dataSource.nextPage;
    
    NSNumber *nextValue = self.dataSource.nextPage;
    
    XCTAssertEqual(nextValue.integerValue, initialValue.integerValue, @"Next page is not equal original value");
}

#pragma mark RepositoryAtIndexPath

- (void)testGivenArrayWit5Repositories_WhenRepositoryAtIndexPath4_ThenGetTheRepository {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
    NSArray <Repository *>*repositories = [self generateRepositories:5];
    self.dataSource.repositories = repositories;
    
    Repository *repository = [self.dataSource repositoryAtIndexPath:indexPath];
    
    XCTAssertEqual(repository, repositories[4], @"Repository is not the correct");
}


- (void)testGivenArrayWit5Repositories_WhenRepositoryAtIndexPath7_ThenGetTheNil {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:7 inSection:0];
    NSArray <Repository *>*repositories = [self generateRepositories:5];
    self.dataSource.repositories = repositories;
    
    Repository *repository = [self.dataSource repositoryAtIndexPath:indexPath];
    
    XCTAssertNil(repository, @"Repository should be nil");
}

- (void)testGivenNoRepositories_WhenRepositoryAtIndexPath7_ThenGetTheNil {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:7 inSection:0];
    
    Repository *repository = [self.dataSource repositoryAtIndexPath:indexPath];
    
    XCTAssertNil(repository, @"Repository should be nil");
}

#pragma mark - Tests data

- (NSArray <Repository *>*)generateRepositories:(NSInteger)aTotal {
    NSMutableArray <Repository *>*mocks = [NSMutableArray new];
    
    for (int i=0; i<aTotal; i++) {
        [mocks addObject:[self generateRepository:i]];
    }
    return mocks.copy;
}

- (Repository *)generateRepository:(int)number {
    Repository *repo = [Repository new];
    
    repo.name = [NSString stringWithFormat:@"Name - %d", number];
    repo.descriptionText = [NSString stringWithFormat:@"This is the repo description text, this could be very long - %d", number];
    repo.loginOwner = [NSString stringWithFormat:@"Owner %d", number];
    repo.isFork = number%2;
    
    return repo;
}

@end
