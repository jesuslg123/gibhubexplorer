//
//  CollectionViewCell.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Repository;

@interface CollectionViewCell : UICollectionViewCell


/**
 Cell text indenfier

 @return NSString Indentifier
 */
+ (nonnull NSString *)cellIdentifier;


/**
 Configure cell data UI

 @param aRepository Repository Repository instance
 */
- (void)configureCellWithRepository:(nonnull Repository *)aRepository;

@end
