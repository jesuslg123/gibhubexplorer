//
//  RepositoriesNetworkManager.h
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Repository;

@interface RepositoriesNetworkRquest : NSObject


/**
 Rquest repositories GitHub end point

 @param aArguments NSDictionary Params
 @param block completionBlock
 */
- (void)requestRepositoriesWithArguments:(nullable NSDictionary *)aArguments completionBlock:(void (^_Nullable)(id _Nullable error, NSArray <Repository *>* _Nullable result))block;

@end
