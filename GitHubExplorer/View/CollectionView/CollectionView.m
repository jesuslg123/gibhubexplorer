//
//  CollectionView.m
//  GitHubExplorer
//
//  Created by Jesus Lopez Garcia on 17/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

#import "CollectionView.h"

@implementation CollectionView

+ (CollectionView *)basicCollectionView {
    
    CollectionView *collectionView = [[CollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:[self collectionViewFlow]];
    [collectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [collectionView setBackgroundColor:[UIColor clearColor]];
    
    return collectionView;
    
}

+ (UICollectionViewFlowLayout *)collectionViewFlow {
    
    UICollectionViewFlowLayout* collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    [collectionViewFlowLayout setItemSize:CGSizeMake(200, 200)];
    [collectionViewFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    return collectionViewFlowLayout;
    
}

@end
